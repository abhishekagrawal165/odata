const CONSTANTS = require('../../config/constants');

const apiSuccessRes = (req, res, message = CONSTANTS.DATA_NULL, data = CONSTANTS.DATA_NULL, code = CONSTANTS.ERROR_CODE_ZERO, error = CONSTANTS.ERROR_FALSE, token = CONSTANTS.DATA_NULL) => {

	return res.status(200).json({
		message: message,
		code: code,
		error: error,
		data: data,
		token: token
	});
};

const apiErrorRes = (req, res, message = CONSTANTS.DATA_NULL, data = CONSTANTS.DATA_NULL, code = CONSTANTS.ERROR_CODE_ONE, error = CONSTANTS.ERROR_TRUE) => {
	return res.status(200).json({
		message: message,
		code: code,
		error: error,
		data: data
	});
};

const currentDate = () => {
	return parseInt(Date.now()/1000);
};

const getReferer = (req) =>{
	return req.header('Referer') || CONSTANTS.ADMIN_URL;
}

module.exports = {
	apiSuccessRes,
	apiErrorRes,
	currentDate,
	getReferer
};

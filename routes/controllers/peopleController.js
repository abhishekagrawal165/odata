const express = require('express');
const router = express.Router();
const Joi = require('@hapi/joi');
const axios = require('axios');
//common files
const constants = require('../../config/constants');
const globalFunction = require('../utils/globalFunction');
const apiSuccessRes = globalFunction.apiSuccessRes;
const apiErrorRes = globalFunction.apiErrorRes;

/* GET INDEX. */
router.get('/', async function (req, res) {
  try {
    let peopleList = [];

    //extracting search params
    let {
      peopleName
    } = req.query;
	
	//preparing the request URI
	let peopleRequestURI = constants.BASE_API_URL + '/People';
	if(peopleName){
		peopleRequestURI += '?$filter=' + peopleName;
	}
	//fetching data
	let fetchPeople = await axios.get(peopleRequestURI);
	if(fetchPeople.status === constants.SUCCESS){
		console.log(fetchPeople.data);
		peopleList = fetchPeople.data.value;
	}
	
    //rendering the view
    res.render('people/index', { peopleList: peopleList, peopleName: peopleName, page_title: "People List", tab_open: "people", flash_success_message: req.flash('success_message'), flash_error_message: req.flash('error_message') });
  } catch (error) {
    console.log(error);
    req.flash("error_message", "Something went wrong while fetching people!");
    //sending to previous page
    res.redirect(globalFunction.getReferer(req));
  }
});

router.post('/getPeopleItem', async function (req, res) {
  try {
	//defining validation requirements for inputs
	const schema = Joi.object({
		userName: Joi.string().required().messages({"string.base": "User name must be a string", "äny.required": "User name is required."})
	});
	//validating inputs
	try {
		await schema.validateAsync(req.body, {
			abortEarly: true
		});
	}
	catch (error) {
		console.log(error);
		return apiErrorRes(req, res, error.details[0].message);
	}
	
    //extracting params
    let {
      userName
    } = req.body;
	
	
	//preparing the request URI
	let peopleRequestURI = constants.BASE_API_URL + "/People('" + userName + "')";
	//fetching data
	let fetchPeople = await axios.get(peopleRequestURI);
	if(fetchPeople.status === constants.SUCCESS){
		console.log(fetchPeople.data);
		let peopleData = fetchPeople.data;
		return apiSuccessRes(req, res, "Data fetched!", peopleData);
	}
	return apiErrorRes(req, res, "Not found!");
  } catch (error) {
    console.log(error);
	return apiErrorRes(req, res, "Something went wrong!");
  }
});

module.exports = router;

//var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors')

var flash = require('req-flash');
var constants = require('./config/constants');

//default routers
var defaultRouter = require(constants.CONTROLLER_PATH + 'defaultController');
var peopleRouter = require(constants.CONTROLLER_PATH + 'peopleController');

var app = express();
var session = require('express-session');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//session handler
app.use(
  session({
    resave: true,
    saveUninitialized: true,
    secret: 'XCR3rsasa%RDHHH',
    cookie: { maxAge: 28800000 }
  })
);

//cross origin allow
app.use(cors());
app.use(flash());
//disable X-Powered-By header
app.disable('x-powered-by')

//default routing
app.use('/', defaultRouter);
app.use('/people', peopleRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  //next(createError(404));
  return res.status(404).json({
    message: "404 Not Found",
    code: constants.ERROR_CODE_ONE,
    error: constants.ERROR_TRUE,
    data: constants.DATA_NULL
  });
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//some local variables to be used in views
app.locals.site = { title: 'Odata Implementation', copyright: 'Copyright @ Odata Implementation', site_url: constants.SITE_URL};

module.exports = app;

# README #

This repository contains the implementation of Odata REST APIs.

### How do I get set up? ###

* First run `npm install` in the folder.
* Then Run `npm start`.
* Access application via http://localhost:3001/people
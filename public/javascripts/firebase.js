<script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-database.js"></script>

<script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-analytics.js"></script>

<script>    
    // Your web app's Firebase configuration
    let firebaseConfig = {
        apiKey: "AIzaSyC41SC-1Dl-OXx0hJwbxvQ3MiXwhJOj2aA",
        authDomain: "fir-chat-3e109.firebaseapp.com",
        databaseURL: "https://fir-chat-3e109.firebaseio.com",
        projectId: "fir-chat-3e109",
        storageBucket: "fir-chat-3e109.appspot.com",
        messagingSenderId: "698498773172",
        appId: "1:698498773172:web:0dbf92ab173a390a2ba7e1",
        measurementId: "G-B1DGYW9Q9Y"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
</script>
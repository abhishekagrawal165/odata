module.exports = Object.freeze({
	BASE_API_URL : 'https://services.odata.org/TripPinRESTierService',
	DATA_NULL: null,
	ERROR_TRUE: true,
	ERROR_FALSE: false,
	ERROR_CODE_TWO: 2,
	ERROR_CODE_ONE: 1,
	ERROR_CODE_ZERO: 0,
	NOT_FOUND: 404,
	SERVER_ERROR: 500,
	SUCCESS: 200,
	SITE_URL:"http://localhost:3001/",
	CONTROLLER_PATH:"./routes/controllers/"
});
